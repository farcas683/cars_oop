#pragma once
#include "CarsController.h"
class UserInterface
{
private:
	CarsController m_controller;
public:
	UserInterface(CarsController& controller);
	void addCars();
	void removeCars();
	void show();
	void randomAdd();
	void redo();
	void undo();
};

