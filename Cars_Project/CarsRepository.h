#pragma once
#include <vector>
#include <algorithm>
#include "Cars.h"
class CarsRepository
{
private:
	vector<Cars*> m_repo;
public:
	void addCars(Cars* car);
	Cars* removeCars(int id);
	ostream& display(ostream& os, bool(*filterfunction)(Cars* car));
};

