#include "UserInterface.h"
#include <iostream>
#include <string>
#include "Cars.h"
#include <random>
UserInterface::UserInterface(CarsController& controller):m_controller{controller}
{

}

void UserInterface::addCars(){
	int id=0, year=0;
	bool err;
	string model;
	do {
		err = false;
		cout << "Please insert the id" << endl;
		cin >> id;
		if (cin.fail())
		{
			cout << "The id should be an integer!!!!" << endl;
			err = true;
			cin.clear();
			cin.ignore(80, '\n');
		}
	} while (err == true);

	err = false;
	do {
		err = false;
		cout << "Please insert the model" << endl;
		cin >> model;
		if (cin.fail())
		{
			cout << "Enter a valid model!!!!" << endl;
			err = true;
			cin.clear();
			cin.ignore(80, '\n');
		}
	} while (err == true);

	err = false;
	do {
		err = false;
		cout << "Please insert the year " << endl;
		cin >> year;
		if (cin.fail())
		{
			cout << "The year should be an integer!!!!" << endl;
			err = true;
			cin.clear();
			cin.ignore(80, '\n');
		}
	} while (err == true);

	Cars* car = new Cars(id, model, year);
	m_controller.AddCar(car);
}
void UserInterface::randomAdd()
{
	string models[20] = {"A6","A4","A7","A7","X3","X5","X1","E36","E92","GLA","GLK","911","RS7","AMG C63","Golf 7","Golf R","M440i","S-Class"};
	int id = rand() % 100;
	int year = rand() % 30 + 1980;
	int model_index = rand() % 18;
	string model = models[model_index];
	Cars* car = new Cars(id, model, year);
	m_controller.AddCar(car);
}

void UserInterface::redo()
{
	m_controller.redoOnce();
}

void UserInterface::undo() {
	m_controller.redoOnce();
}

void UserInterface::removeCars()
{
	int id;
	cout << "Please insert the id you want to remove: ";
	cin >> id;
	m_controller.removebyid(id);

}

void UserInterface::show()
{
	char option;
	do {
		cout << "Please insert your option:" << endl;
		cout << "\t a - add " << endl << "\t r - remove" << endl << "\t d - display" << endl << "\t e - exit" << endl
			<< "\t o - redo" << endl << "\t u - undo" << endl;
		cin >> option;
		switch (option)
		{
		default:
			break;
		case'a':
			char opt1;
			cout << "\t a - standard add" << endl;
			cout << "\t r - random add" << endl;
			cout << "\t d - add details" << endl;
			cin >> opt1;
			switch (opt1) {
			default:
				break;
			case'a':
				addCars();
				break;
			case'r':
				randomAdd();
				break;
			}
			break;
		case'r':
			removeCars();
			break;
		case'e':
			exit(0);
			break;
		case'd':
			char opt2;
			cout << "\t d - display all" << endl;
			cout << " \t f - filter display" << endl;
			cout << "Please choose how you want to display the items" << endl;
			cin >> opt2;
			switch (opt2) {
			default:
				break;
			case'd':
				m_controller.displayAll();
				break;
			case'f':
				string model;
				cout << "Provide a model" << endl;
				cin >> model;
				m_controller.filterDisplay(model);
				break;
			}
			break;
		case 'o':
			redo();
			break;
		case'u':
			undo();
		}
	} while (true);
}

