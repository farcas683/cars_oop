#include "SportCars.h"
#include <iomanip>
SportCars::SportCars(int id, string model, int year, int quartermile_time):Cars(id,model,year),m_quartermile_time{quartermile_time}
{
	if (m_quartermile_time < 11)
		m_body.push_back(BodyType::HATCHBACK);
	else if (m_quartermile_time < 13)
		m_body.push_back(BodyType::COUPE);
}

void SportCars::display(ostream& os) const
{
	Cars::display(os);
	os << setw(2) << m_quartermile_time << " | ";
}
