#include "Cars.h"
#include <iomanip>
Cars::Cars(int id, string model,int year)
{
	this->m_id = id;
	this->m_model = model;
	this->m_year = year;
}

Cars::Cars()
{
	this->m_id = 0;
	this->m_model = "";
	this->m_year = 0;
}

Cars& Cars::operator=(const Cars& car)
{
	this->m_id = car.m_id;
	this->m_model = car.m_model;
	this->m_year = car.m_year;
	return *this;
}

void Cars::display(ostream& os) const
{
	os << setw(2) << m_id << " | " << setw(7)<<m_model << " | " << setw(4) << m_year << " | ";
//	os << m_id << "," << m_model << "," << m_year << ",";
}

ostream& operator<<(ostream& os, const Cars& car)
{
	car.display(os);
	return os;
}
