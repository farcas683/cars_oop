#include "LuxuryCars.h"
#include <iomanip>
LuxuryCars::LuxuryCars(int id, string model, int year, int ComfortLevel):Cars(id,model,year),m_ComfortLevel{ComfortLevel}
{
	if (m_ComfortLevel > 900)
		m_body.push_back(BodyType::COUPE);
	else if (m_ComfortLevel > 600)
		m_body.push_back(BodyType::SUV);
	else m_body.push_back(BodyType::HATCHBACK);
}

void LuxuryCars::display(ostream& os) const
{
	Cars::display(os);
	os << setw(4) << m_ComfortLevel << " | ";
}
