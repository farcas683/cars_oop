#include "CarsController.h"
#include <iostream>
#include<ostream>
CarsController::CarsController(CarsRepository& r)
{
	m_repo = r;
}

void CarsController::AddCar(Cars* car)
{
	m_repo.addCars(car);
	m_undoStack.push(std::make_pair(ActionType::ADD, car));
}

void CarsController::removebyid(int carid)
{
	Cars* car = m_repo.removeCars(carid);
	if(car)
		m_undoStack.push(std::make_pair(ActionType::REMOVE, car));
}

void CarsController::displayAll()
{
	m_repo.display(std::cout, [](Cars* car)->bool {return true; });
}

void CarsController::undoOnce()
{
	int id, year;
	string model;
	id = m_undoStack.top().second->getid();
	model = m_undoStack.top().second->getmodel();
	year = m_undoStack.top().second->getYear();
	Cars* car = new Cars(id, model, year);
	m_repo.addCars(car);
	m_undoStack.pop();
	m_redoStack.push(std::make_pair(ActionType::UNDO, car));
}

void CarsController::redoOnce()
{
	int id, year;
	string model;
	id = m_redoStack.top().second->getid();
	model = m_redoStack.top().second->getmodel();
	year = m_redoStack.top().second->getYear();
	Cars* car = new Cars(id, model, year);
	m_repo.addCars(car);
	m_redoStack.pop();
	m_undoStack.push(std::make_pair(ActionType::REDO, car));
}

void CarsController::filterDisplay(string model)
{
	//m_repo.display(std::cout, [model](Cars* car)->bool {return car->getmodel() == model; });
}
