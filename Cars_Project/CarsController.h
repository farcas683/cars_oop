#pragma once
#include "CarsRepository.h"
#include <stack>
using namespace std;
enum class ActionType {
	ADD,
	REMOVE,
	UNDO,
	REDO
};
class CarsController
{
private:
	CarsRepository m_repo;
	stack<pair<ActionType, Cars*>>m_undoStack;
	stack<pair<ActionType, Cars*>>m_redoStack;
public:
	CarsController(CarsRepository& r);
	void AddCar(Cars* car);

	void removebyid(int carid);
	void displayAll();
	void undoOnce();
	void redoOnce();
	void filterDisplay(string model);
};

