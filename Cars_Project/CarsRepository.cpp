#include "CarsRepository.h"

void CarsRepository::addCars(Cars* car)
{
	m_repo.push_back(car);
	sort(m_repo.begin(), m_repo.end(), [](Cars* first,Cars* second) {
		return first->getid() < second->getid();
		});
}

Cars* CarsRepository::removeCars(int id)
{
	Cars* car = nullptr;
	auto index = find_if(m_repo.begin(), m_repo.end(), [id](Cars* car)->bool {
		return car->getid() == id;
		});
	if (index != m_repo.end()) {
		car = *index;
		m_repo.erase(index);
	}
	return car;
}

ostream& CarsRepository::display(ostream& os, bool(*filterfunction)(Cars* car))
{
	for (auto i = 0; i < m_repo.size(); i++)
		if (filterfunction(m_repo[i]) == true)
			os << *m_repo[i] << endl;
	return os;
}
