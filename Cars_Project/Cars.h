#pragma once
#include <string>
#include <ostream>
#include<vector>
using namespace std;
enum class BodyType {
	SUV,
	COUPE,
	HATCHBACK
};
class Cars
{
public:
	Cars(int id, string model,int year);
	Cars();
	int getid()const { return this->m_id; };
	int getYear()const { return this->m_year; };
	string getmodel()const {return this->m_model;}
	Cars& operator= (const Cars& car);
	virtual void display(ostream& os)const;
	friend ostream& operator<<(ostream& os, const Cars& car);
private:
	int m_id;
protected:
	string m_model;
	vector<BodyType> m_body;
	int m_year;

};

